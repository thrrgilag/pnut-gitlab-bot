FROM python:3.8-slim-bullseye AS builder

RUN pip install --no-cache-dir --upgrade pip setuptools wheel

WORKDIR /usr/src/app

COPY . .

RUN pip wheel . --wheel-dir /wheels --find-links /wheels



FROM python:3.8-slim-bullseye AS run

WORKDIR /app

COPY --from=builder /wheels /wheels
COPY entrypoint.sh .

RUN pip --no-cache-dir install --find-links /wheels --no-index pnut-gitlab-bot

VOLUME /data
WORKDIR /data
EXPOSE 4004/tcp
ENTRYPOINT [ "/app/entrypoint.sh" ]
