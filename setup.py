from setuptools import setup

setup(
    name='pnut_gitlab_bot',
    version='0.2.0',
    py_modules=[
        'pnut_gitlab.models',
        'pnut_gitlab.pnutbot',
        'pnut_gitlab.webhook',
    ],
    install_requires=[
        'PyYAML',
        'pnutpy',
        'Flask',
        'SQLAlchemy',
        'websocket-client',
        'gunicorn',
        'python-gitlab',
    ],
    entry_points={
        'console_scripts': [
            'pnut-gitlab-bot = pnut_gitlab.pnutbot:main'
        ]
    },
)

